package com.sz.test2;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * 生产者
 */
public class Test {
    private static final String ACTIVEMQ="tcp://192.168.182.130:61616";
    private static final String TOPIC="topic";
    public static void main(String[] args) throws Exception{
        //创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory=new ActiveMQConnectionFactory(ACTIVEMQ);

        //获得连接
        Connection connection = activeMQConnectionFactory.createConnection();
        //启动访问
        connection.start();
        //创建会话

        //参数： 第一个是事物，第二个是签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 创建目的地     分为主题和队列，  这里是创建主题
        Topic topic = session.createTopic(TOPIC);

        //创建消息的生产者
        MessageProducer producer = session.createProducer(topic);


        TextMessage xxxx = session.createTextMessage("消息订阅");   //创建消息
        producer.send(xxxx);//发送
        producer.close();
        session.close();
        connection.close();

        System.out.println("发生消息");

    }
}
