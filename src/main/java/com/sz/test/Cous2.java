package com.sz.test;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 *  使用监听的方式获取数据  监听器
 *  异步非阻塞
 */
public class Cous2 {
    private static final String ACTIVEMQ="tcp://192.168.182.130:61616";
    private static final String QUEUE="que1";
    public static void main(String[] args)throws Exception {

        //创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory=new ActiveMQConnectionFactory(ACTIVEMQ);

        //获得连接
        Connection connection = activeMQConnectionFactory.createConnection();
        //启动访问
        connection.start();
        //创建会话

        //参数： 第一个是事物，第二个是签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 创建目的地     分为主题和队列，  这里是创建队列
        Queue queue = session.createQueue(QUEUE);

        MessageConsumer consumer = session.createConsumer(queue);

        consumer.setMessageListener(new MessageListener() {
            @Override
            public void onMessage(Message message) {
                if (null !=message && message instanceof TextMessage){
                    TextMessage textMessage= (TextMessage)message;
                    try {
                        String text = textMessage.getText();
                        System.out.println(text);
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        System.out.println("访问结束");
        System.in.read();
        consumer.close();
        session.close();
        connection.close();
    }


    //当先启动一个服务方，然后在启动消费方，是可以被消费到数据的。
    //当先启动一个服务方，随后启动两个消费方(注意，消费方启动有顺序的)，那么只有前面先启动的消费方才可以得到数据。
    //如果是先启动两个消费方，然后在启动服务方，在服务方启动前，消费方会监听消息，当服务方启动后发送数据
    //那么就会使用轮询的方式让消费方得到值。
}
