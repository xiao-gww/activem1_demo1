package com.sz.test;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 *
 *  同步 阻塞
 */

public class Cous {
    private static final String ACTIVEMQ="tcp://192.168.182.130:61616";
    private static final String QUEUE="que1";
    public static void main(String[] args)throws Exception {

        //创建连接工厂
        ActiveMQConnectionFactory activeMQConnectionFactory=new ActiveMQConnectionFactory(ACTIVEMQ);

        //获得连接
        Connection connection = activeMQConnectionFactory.createConnection();
        //启动访问
        connection.start();
        //创建会话

        //参数： 第一个是事物，第二个是签收
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // 创建目的地     分为主题和队列，  这里是创建队列
        Queue queue = session.createQueue(QUEUE);

        MessageConsumer consumer = session.createConsumer(queue);

        TextMessage textMessage=(TextMessage)consumer.receive();  //还有一个带参数的方法设置时间，当到了时间就会断开连接
        System.out.println(textMessage.getText());
        System.out.println("访问结束");
        consumer.close();
        session.close();
        connection.close();
    }
}
